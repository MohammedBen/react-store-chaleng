import { useSelector } from "react-redux";
import  "./cart.scss";
import Product from "../product/product";



const Cart: React.FC = () => {
    const products = useSelector((state: any) => state.data.cart);
    const totalPrice: number = (products.length > 0) ?products.reduce((total:any, product:any) => {
        const itemTotal = product?.price * product?.count;
        return total + itemTotal;
      }, 0) : 0;
    return (
    <div>
        
        Total : {totalPrice || 0}
        
        {!!products ?products.map((item: any) => (
        <div className='elm' key={item?.id}>
            <Product key={item?.id} id={item?.id} name={item?.name} price={item?.price} rating={item?.rating} image={item?.image} description= {item?.description} showDetails={true} showDetailsInCart={true} category='' count={item?.count}/>
        </div>
          )) : null}
    </div>)
}

export default Cart