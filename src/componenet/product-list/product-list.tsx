import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProduct } from '../../redux/actions/actions';
import Product from '../product/product';
import './product-list.scss';
import { CircularProgress } from '@mui/material';

const PoductListE: React.FC = () => {
  const dispatch = useDispatch();
  const data: any[] = useSelector((state: any) => state.data.data);
  const error = useSelector((state: any) => state.data.error);
  const loading = useSelector((state: any) => state.data.loading);

  useEffect(() => {
    dispatch(getAllProduct() as any);
  }, [dispatch]);


  if (loading) {
    return <div className='loading'><CircularProgress disableShrink /></div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  return (
    <div className='bloc-main'>
      <h1>Product List :</h1>
      <div className='list-product'>
          {!!data ?data.map((item: any) => (
        <div className='elm' key={item.id}>
            <Product key={item.id} id={item.id} name={item.title} price={item.price} rating={item.rating.rate} image={item.image} description='' showDetails={false} showDetailsInCart={false} category=''  count={0}/>
        </div>
          )) : null}
      </div>
    </div>
  );
};

export default PoductListE;
