import React from 'react';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { Fab, Rating } from '@mui/material';
import './product.scss';
import { useDispatch } from 'react-redux';
import { addItemToCartSuccess, deletItemFromCartSuccess } from '../../redux/actions/actions';
import { PRODUCT } from '../../interfaces/product.interface';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import DeleteIcon from '@mui/icons-material/Delete';


interface ProductProps {
  name: string;
  price: number;
  id: number;
  rating: number;
  image: string;
  category: string | null;
  description: string | null;
  showDetails: boolean | null;
  showDetailsInCart: boolean | null;
  count: number | null;
}


const Product: React.FC<ProductProps> = ({id, name, price, rating, image, description, showDetails, category, showDetailsInCart,count }) => {
  const dispatch = useDispatch();
  let Count = count;
  const addProductToCart = () => {
    const product: PRODUCT = {id, name, price, rating, image, description, category,count:1 };
    dispatch(addItemToCartSuccess(product))
  }
  
  const addCount = () => {
    addProductToCart();
  }
  const decreasProduct = () => {
    const product: PRODUCT = {id, name, price, rating, image, description, category,count:1 };
    dispatch(deletItemFromCartSuccess(product,1));
  }

  const DeletedProduct = () => {
    const product: PRODUCT = {id, name, price, rating, image, description, category,count };
    dispatch(deletItemFromCartSuccess(product,count!));
  }

  return (
    <div className='bloc-product'>
      <p className='img'><img src={image} /></p>
      <div className='details-product'>
        <p className='title'>Name: {name}</p>
        <p className='price'>price: {price}</p>
        <p className='rating'><Rating name="read-only" value={rating} readOnly /></p>
        {showDetails ? <p className='category'> {category}</p> : null}
        {showDetails ? <p className='description'>description: {description}</p> : null}
        {(showDetails && !showDetailsInCart) ? <div className='cart'><AddShoppingCartIcon onClick={addProductToCart} /> </div> :
          <div className='number'>
            <div>
              <Fab size="small" color="secondary" aria-label="add" onClick={decreasProduct}>
                <RemoveIcon />
              </Fab>
            </div>
            <div> {count}</div>
            <div>
              <Fab size="small" color="secondary" aria-label="add" onClick={addCount}>
                <AddIcon />
              </Fab>
            </div>
            <div>
              <Fab size="small" color="secondary" aria-label="add" onClick={DeletedProduct}>
                <DeleteIcon />
              </Fab>
            </div>
          </div>}
      </div>
    </div>
  );
};

export default Product;
