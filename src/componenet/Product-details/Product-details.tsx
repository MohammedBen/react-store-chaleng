
import { useEffect } from 'react';
import './Product-details.scss';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getProductById } from '../../redux/actions/actions';
import Product from '../product/product';



const ProductDetails: React.FC = () => {
    const dispatch = useDispatch();
    const { id } = useParams<{ id: string }>();
    const product = useSelector((state: any) => (state?.data?.product) ? state.data.product : null);
  
    useEffect(() => {
      // Dispatch the action to fetch the product by ID
      dispatch(getProductById(id!) as any);
    }, [dispatch, id]);
  
    return (
        <div>
            
            {(product && product.rating) ?<Product key={product.id} id={product.id} name={product.title} price={product.price} rating={product?.rating.rate} image={product.image} description={product.description} showDetails={true} showDetailsInCart={false} category={product.category} count={0} /> :null}
 
        </div>
    )
}

export default ProductDetails;