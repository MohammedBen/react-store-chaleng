import React from 'react';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import './nav-bar.scss';
import { useSelector } from 'react-redux';

const Navbar: React.FC = () => {

  const goToCart = () => {
    if (cart.length > 0)
    window.location.href= '/cart';
  };

  const cart = useSelector((state: any) => state.data.cart);

  return (
    <div className='bloc-nav-bar'>
      <div className='cart' onClick={goToCart}><ShoppingCartIcon /> </div>
      {(cart && cart.length > 0) ? <div className='counter'><span>{cart.length}</span></div> : null}
    </div>
  );
};

export default Navbar;
