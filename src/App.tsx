import { BrowserRouter as Router,  Route, Routes } from 'react-router-dom';
import './App.scss';
import PoductListE from './../src/componenet/product-list/product-list';
import ProductDetails from './componenet/Product-details/Product-details';
import Cart from './componenet/cart/cart';

function App() {
  return (
      <Router>
      <Routes>
        <Route path="/" element={<PoductListE />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/product/:id" element={<ProductDetails />} />
      </Routes>
      </Router>
  );
}

export default App;
