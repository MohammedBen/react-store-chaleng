import { combineReducers } from 'redux';
import {
  GET_ALL_PRODUCT_REQUEST,
  GET_ALL_PRODUCT_SUCCESS,
  GET_ALL_PRODUCT_FAILURE,
  GET_PRODUCT_BY_ID_REQUEST,
  GET_PRODUCT_BY_ID_SUCCESS,
  GET_PRODUCT_BY_ID_FAILURE,
  INCREASING_IN_CART_SUCCESS,
  DECREASING_FROM_CART_SUCCESS
} from '../actions/actionsTypes';
import { LOCAL_STORAGE } from '../../interfaces/product.interface';
let localStorage = { ...JSON.parse(window.localStorage.getItem(LOCAL_STORAGE)!) };
if (!localStorage?.cart)
  localStorage = { ...localStorage, cart: [] }
// {
//   loading: false,
//   data: [],
//   product: {},
//   cart: [],
//   error: '',
// };
const initialState = localStorage


const dataReducer = (state = initialState, action: any) => {

  switch (action.type) {
    case GET_ALL_PRODUCT_REQUEST:
      return { ...state, loading: true };
    case GET_ALL_PRODUCT_SUCCESS:
      const newStateAllProduct = { ...state, loading: false, data: action.payload, error: '' };
      window.localStorage.setItem(LOCAL_STORAGE, JSON.stringify(newStateAllProduct));
      return newStateAllProduct;
    case GET_ALL_PRODUCT_FAILURE:
      return { ...state, loading: false, data: [], error: action.payload };
    case GET_PRODUCT_BY_ID_REQUEST:
      return { ...state, loading: true };
    case GET_PRODUCT_BY_ID_SUCCESS:
      const newStateProduct = { ...state, loading: false, product: action.payload, error: '' };
      window.localStorage.setItem(LOCAL_STORAGE, JSON.stringify(newStateProduct));
      return newStateProduct;
    case GET_PRODUCT_BY_ID_FAILURE:
      return { ...state, loading: false, product: {}, error: action.payload };
    case DECREASING_FROM_CART_SUCCESS:

    let cartProduct = [];
      if (state.cart.length > 0) {
        let products: any[] = state.cart;
        const [product] = products.filter(product => product.id === action.payload.data.id);
        cartProduct = products.map(elm => {
          if (elm.id === product.id){
            if(elm.count > 1 && (product.count - action.payload.qnt) !== 0)
            return { ...product, count: product.count - action.payload.qnt }
          return ;
          }
          return product;
        })
      }
      const newStateDelProductCart = { ...state, loading: false, cart: [...cartProduct], error: '' };
      window.localStorage.setItem(LOCAL_STORAGE, JSON.stringify(newStateDelProductCart));
      return newStateDelProductCart;
    case INCREASING_IN_CART_SUCCESS:
      let newCartProduct = [];
      if (state.cart.length > 0) {
        let products: any[] = state.cart;
        const [product] = products.filter(product => product.id === action.payload.id)
        if (product) {
          newCartProduct = products.map(elm => {
            if (elm.id === product.id)
            return { ...product, count: product.count+1 }
          return product;
        })
      } else {
        newCartProduct = [ ...products,{ ...action.payload }]
      }
    }else{
      
      newCartProduct = [ { ...action.payload }]
    }
      const newStateAddProductCart = { ...state, loading: false, cart: [...newCartProduct], error: '' };
      window.localStorage.setItem(LOCAL_STORAGE, JSON.stringify(newStateAddProductCart));
      return newStateAddProductCart;
    default:
      return state;
  }

};

const rootReducer = combineReducers({
  data: dataReducer,
});

export default rootReducer;
