import { Dispatch } from 'redux';
import {
  GET_ALL_PRODUCT_FAILURE,
  GET_ALL_PRODUCT_REQUEST,
  GET_ALL_PRODUCT_SUCCESS,
  GET_PRODUCT_BY_ID_REQUEST,
  GET_PRODUCT_BY_ID_SUCCESS,
  GET_PRODUCT_BY_ID_FAILURE,
  INCREASING_IN_CART_SUCCESS,
  DECREASING_FROM_CART_SUCCESS
} from './actionsTypes';
import { PRODUCT } from '../../interfaces/product.interface';

export const getAllProductRequest = () => ({
  type: GET_ALL_PRODUCT_REQUEST,
});

export const getAllProductSuccess = (data: any) => {
  return ({
    type: GET_ALL_PRODUCT_SUCCESS,
    payload: data,
  })
};

export const getAllProductFailure = (error: string) => ({
  type: GET_ALL_PRODUCT_FAILURE,
  payload: error,
});

export const getProductByIdRequest = () => ({
  type: GET_PRODUCT_BY_ID_REQUEST,
});

export const getProductByIdSuccess = (data: any) => {
  return ({
    type: GET_PRODUCT_BY_ID_SUCCESS,
    payload: data,
  })
};

export const getProductByIdFailure = (error: string) => ({
  type: GET_PRODUCT_BY_ID_FAILURE,
  payload: error,
});

export const addItemToCartSuccess = (data:PRODUCT ) => {
  return ({
    type: INCREASING_IN_CART_SUCCESS,
    payload: data,
  })
};

export const deletItemFromCartSuccess = (data:PRODUCT,qnt:number) => {
  return ({
    type: DECREASING_FROM_CART_SUCCESS,
    payload: {data,qnt},
  })
};


export const saveState = (data:any) => {
  return ({
    type: '',
    payload: data,
  })
};

export const getAllProduct = () => {
  return async (dispatch: Dispatch) => {
    dispatch(getAllProductRequest());
    try {
      const response = await fetch('https://fakestoreapi.com/products');
      const products = await response.json();
      dispatch(getAllProductSuccess(products));
    } catch (error: any) {
      dispatch(getAllProductFailure(error.message));
    }
  };
};

export const getProductById = (id:string) => {
  return async (dispatch: Dispatch) => {
    dispatch(getAllProductRequest());
    try {
      const response = await fetch('https://fakestoreapi.com/products/'+id);
      const product = await response.json();
      dispatch(getProductByIdSuccess(product));
    } catch (error: any) {
      dispatch(getProductByIdFailure(error.message));
    }
  };
};

export const addItemToCart = (item:any)=> {
  return async (dispatch: Dispatch) => {
      dispatch(addItemToCartSuccess(item));
  };
}

export const deletItemFromCart = (item:any)=> {
  debugger;
  return async (dispatch: Dispatch) => {
      // dispatch(deletItemFromCartSuccess(item));
  };
}
