export interface PRODUCT{ id: number;name: string; price: number; rating: number; image: string; description: string | null; category: string | null; count: number | null;};

export const LOCAL_STORAGE = 'state';